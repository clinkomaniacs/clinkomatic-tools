//
//  main.swift
//  ClinkomaticTools
//
//  Created by Giuseppe de Santis on 19/01/16.
//  Copyright © 2016 Gidesan. All rights reserved.
//

import Foundation

func processEmails(sourceDir: String, sourceFileName: String, emailsToRemoveFileName: String, splitSize: Int) {
    
    let workingDir = NSString(string:sourceDir).stringByExpandingTildeInPath
    let destFileNamePattern = "email-list.txt"
    
    let emailRegExPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    
    do {
    
	    let emails = try extractMatchesFromFile(emailRegExPattern, sourcePath: "\(workingDir)/\(sourceFileName)")
        let sourceCount = emails.count
        print("\(sourceCount) emails from \(sourceFileName)")
        
	    let emailsToRemove = try extractMatchesFromFile(emailRegExPattern, sourcePath: "\(workingDir)/\(emailsToRemoveFileName)")
        print("\(emailsToRemove.count) emails to remove from \(emailsToRemoveFileName)")
        
        var uniqueEmails = Array(Set(emails)).sort()
        let uniqueEmailsBeforeRemovalCount = uniqueEmails.count
        print("\(sourceCount - uniqueEmailsBeforeRemovalCount) duplicates removed")
        
        uniqueEmails.removeObjectsInArray(emailsToRemove)
        print("\(uniqueEmailsBeforeRemovalCount - uniqueEmails.count) emails removed from \(emailsToRemoveFileName)")
        print("\(uniqueEmails.count) emails processed")        
    
    	let emailLists = splitArray(uniqueEmails, splitSize: splitSize)
    
	    try writeListsToFile(emailLists, workingDir: workingDir, destFileNamePattern: destFileNamePattern)
        
	} catch let error as NSError {
    	print("Error: \(error)")
	}
    
    print("Completed :)")
}

func writeListsToFile(emailLists: [[String]], workingDir: String, destFileNamePattern: String) throws {
    let nssDestFileNamePattern = NSString(string: destFileNamePattern)
    let fileNameWithoutExtension = nssDestFileNamePattern.stringByDeletingPathExtension
    let ext = nssDestFileNamePattern.pathExtension
    
    var i = 1;
    for emailList in emailLists {
        let output = emailList.reduce("", combine: { (existing, toAppend) in
            if existing.isEmpty {
                return toAppend
            }
            else {
                return "\(existing)\n\(toAppend)"
            }
        })
        
        let destPath = "\(workingDir)/\(fileNameWithoutExtension)-\(i).\(ext)"
        try output.writeToFile(destPath, atomically: true, encoding: NSUTF8StringEncoding)
        i++
    }
}

func extractMatchesFromFile(regexPattern: String!, sourcePath: String!) throws -> [String] {
    let filemgr = NSFileManager.defaultManager()
    if !filemgr.fileExistsAtPath(sourcePath) {
        print("File does not exist!")
        return []
    }
    let data = try String(contentsOfFile: sourcePath, encoding: NSUTF8StringEncoding)

    return extractMatchesFromText(regexPattern, text: data)
}

func extractMatchesFromText(regexPattern: String!, text: String!) -> [String] {
    let regex = try! NSRegularExpression(pattern: regexPattern, options: [.CaseInsensitive])
    let nsString = text as NSString
    let results = regex.matchesInString(text, options: [], range: NSMakeRange(0, nsString.length))
    return results.map({ nsString.substringWithRange($0.range)})
}

func splitArray<T>(s: [T], splitSize: Int) -> [[T]] {
    if s.count <= splitSize {
        return [s]
    } else {
        return [Array(s[0..<splitSize])] + splitArray(Array(s[splitSize..<s.count]), splitSize: splitSize)
    }
}

extension Array where Element: Equatable {
    mutating func removeObject(object: Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
    
    mutating func removeObjectsInArray(array: [Element]) {
        for object in array {
            self.removeObject(object)
        }
    }
}

//processEmails("~/Projects", sourceFileName: "source.txt", emailsToRemoveFileName: "remove.txt", splitSize: 2)
//processEmails("~/Projects", sourceFileName: "fanfbk_newsletter_subscribers.sql", emailsToRemoveFileName: "114-mail-clinker-5-eur.csv", splitSize: 25000)
processEmails("~/Projects", sourceFileName: "fanfbk_newsletter_subscribers.sql", emailsToRemoveFileName: "clinker-30-gen-16.sql", splitSize: 25000)


/*
let lines = data.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
*/